import React from 'react';
import { StyleSheet, FlatList, Text, View, AsyncStorage } from 'react-native';

export default class MoodScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { rows: [] };
  }

  componentWillMount() {
    this.retrieveMoods().then(moodHistory => {
      if (moodHistory.length > 0) {
        const parsedMoodHistory = JSON.parse(moodHistory);

        this.setState({ rows: parsedMoodHistory });
      }
    });
  }

  static navigationOptions = {
    title: 'Mood',
    headerStyle: {
      backgroundColor: '#4A90E2'
    },
    headerTitleStyle: {
      color: '#fff'
    }
  };

  async retrieveMoods() {
    let moodHistory = [];

    try {
      moodHistory = (await AsyncStorage.getItem('moodHistory')) || [];

      return moodHistory;
    } catch (error) {
      console.log('retrieval error');
    }
  }

  extractKey = ({ date }) => date;

  renderItem = ({ item }) => {
    return (
      <Text style={styles.row}>
        Date: {item.date} - Score: {item.score}
      </Text>
    );
  };

  render() {
    return (
      <View style={styles.viewContainer}>
        <FlatList
          data={this.state.rows}
          renderItem={this.renderItem}
          keyExtractor={this.extractKey}
          style={styles.container}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'skyblue',
    flex: 1,
    justifyContent: 'space-between'
  },
  viewContainer: {
    flex: 1,
    backgroundColor: '#fff'
  }
});