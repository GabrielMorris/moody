import React from 'react';
import { View, AsyncStorage, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Settings',
    headerStyle: {
      backgroundColor: '#4A90E2'
    },
    headerTitleStyle: {
      color: '#fff'
    }
  };

  render() {
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return (
      <View style={styles.container}>
        <View style={styles.viewContainer}>
          <Button
            title="Clear Mood History"
            onPress={() => {
              AsyncStorage.clear();
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1
  },
  viewContainer: {
    flex: 1,
    backgroundColor: '#fff'
  }
});
