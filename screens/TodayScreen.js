import React from 'react';
import { RateMood } from '../components/RateMood';

export default class TodayScreen extends React.Component {
  static navigationOptions = {
    title: 'Today',
    headerStyle: {
      backgroundColor: '#4A90E2'
    },
    headerTitleStyle: {
      color: '#fff'
    }
  };

  render() {
    return <RateMood />;
  }
}
