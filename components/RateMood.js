import React from 'react';
import { StyleSheet, View, Text, AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import moment from 'moment';

export class RateMood extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      moodScore: 3,
      hasRatedToday: false
    };
  }

  componentWillMount() {
    const today = moment().format('L');

    this.retrieveMoods()
      .then(moodHistory => {
        const parsedMoodHistory = JSON.parse(moodHistory);

        if (parsedMoodHistory[parsedMoodHistory.length - 1].date === today) {
          // If today has been rated
          console.log('has been rated');
          this.setState({
            moodScore: parsedMoodHistory[parsedMoodHistory.length - 1].score,
            hasRatedToday: true
          });
        } else {
          // Today hasn't been rated
          console.log('hasnt been rated');
          this.setState({ hasRatedToday: false });
        }
      })
      .catch(error => console.error(error));
  }

  incrementMoodScore() {
    console.log(moment().format('L'));
    if (this.state.moodScore < 5) {
      this.setState({ moodScore: this.state.moodScore + 1 });
    }
  }

  decrementMoodScore() {
    if (this.state.moodScore > 1) {
      this.setState({ moodScore: this.state.moodScore - 1 });
    }
  }

  async retrieveMoods() {
    let moodHistory = [];

    try {
      moodHistory = (await AsyncStorage.getItem('moodHistory')) || [];

      return moodHistory;
    } catch (error) {
      console.log('retrieval error');
    }
  }

  async setMoods(array) {
    console.log(array);
    try {
      await AsyncStorage.setItem('moodHistory', array);
    } catch (error) {
      console.log('ERROR');
      console.error(error);
    }
  }

  saveMoodScore(moodScore) {
    let moodHistory = [];

    console.log(`mood score`, moodScore);

    this.retrieveMoods()
      .then(moodsArray => {
        if (moodsArray.length > 0) {
          moodHistory = JSON.parse(moodsArray);
        }
      })
      .then(() => {
        const newUserMoodObject = {
          date: moment().format('L'),
          score: moodScore
        };

        moodHistory.push(newUserMoodObject);
      })
      .then(() => {
        console.log(moodHistory);
        const stringifiedHistory = JSON.stringify(moodHistory);
        this.setMoods(stringifiedHistory);
      })
      .catch(error => console.error(error));
  }

  render() {
    if (this.state.hasRatedToday) {
      // We've already rated today's mood
      return (
        <View style={styles.appView}>
          <View style={styles.boxView}>
            <View style={styles.textView}>
              <Text>
                <Text style={styles.rateMoodTitleTextAlt}>Your mood today</Text>
              </Text>
            </View>
            <View style={styles.textView}>
              <Text style={styles.rateMoodTitleText}>
                {this.state.moodScore}
              </Text>
            </View>
          </View>
        </View>
      );
    } else {
      // We haven't rated today's mood
      return (
        // View for app
        <View style={styles.appView}>
          {/* View for box (centered) */}
          <View style={styles.boxView}>
            {/* View for text */}
            <View style={styles.textView}>
              <Text>
                <Text style={styles.rateMoodTitleTextAlt}>How did </Text>
                <Text style={styles.rateMoodTitleText}>today feel?</Text>
              </Text>
            </View>

            {/* View for rate - row */}
            <View style={styles.rateContainerView}>
              <Button
                title="-"
                buttonStyle={styles.button}
                titleStyle={styles.buttonTitle}
                onPress={() => this.decrementMoodScore()}
              />

              <Text style={styles.text}>{this.state.moodScore}</Text>

              <Button
                title="+"
                buttonStyle={styles.button}
                titleStyle={styles.buttonTitle}
                onPress={() => this.incrementMoodScore()}
              />

              {/* <Button
                title="test set"
                onPress={() => {
                  this.saveMoodScore(this.state.moodScore);
                }}
              />
              <Button
                title="test retrieve"
                onPress={() => {
                  this.retrieveMoods();
                }}
              /> */}
            </View>

            {/* View for button */}
            <View style={styles.buttonView}>
              <Button
                title="Save"
                buttonStyle={styles.saveButton}
                titleStyle={styles.buttonTitle}
                onPress={() => this.saveMoodScore(this.state.moodScore)}
              />
            </View>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  appView: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 200,
    paddingBottom: 200
  },
  boxView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  rateContainerView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  buttonView: {
    flex: 1,
    flexDirection: 'row'
  },
  text: {
    fontSize: 56,
    marginLeft: 25,
    marginRight: 25
  },
  button: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#4A90E2'
  },
  saveButton: {
    width: 75,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#4A90E2',
    marginTop: 50
  },
  buttonTitle: {
    fontWeight: 'bold'
  },
  rateMood: {
    backgroundColor: '#ef553a',
    width: 300,
    paddingTop: 10,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 10
  },
  rateMoodTitleText: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 40,
    marginBottom: 10
  },
  rateMoodTitleTextAlt: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 40,
    marginBottom: 10,
    color: 'gray'
  },
  rateMoodBodyText: {
    color: '#fff',
    fontSize: 16
  }
});
